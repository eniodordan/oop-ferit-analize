﻿// 1. Napravite aplikaciju znanstveni kalkulator koja će imati funkcionalnost
// znanstvenog kalkulatora, odnosno implementirati osnovne (+,-,*,/) i barem 5
// naprednih (sin, cos, log, sqrt...) operacija.

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace LV6___Analiza_1
{
    public partial class form_kalkulator : Form
    {
        double FirstNumber;
        string Operation;

        public form_kalkulator()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            if (textBox_display.Text == "0" && textBox_display.Text != null)
            {
                textBox_display.Text = "1";
            }
            else
            {
                textBox_display.Text = textBox_display.Text + "1";
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            if (textBox_display.Text == "0" && textBox_display.Text != null)
            {
                textBox_display.Text = "2";
            }
            else
            {
                textBox_display.Text = textBox_display.Text + "2";
            }
        }

        private void button3_Click(object sender, EventArgs e)
        {
            if (textBox_display.Text == "0" && textBox_display.Text != null)
            {
                textBox_display.Text = "3";
            }
            else
            {
                textBox_display.Text = textBox_display.Text + "3";
            }
        }

        private void button4_Click(object sender, EventArgs e)
        {
            if (textBox_display.Text == "0" && textBox_display.Text != null)
            {
                textBox_display.Text = "4";
            }
            else
            {
                textBox_display.Text = textBox_display.Text + "4";
            }
        }

        private void button5_Click(object sender, EventArgs e)
        {
            if (textBox_display.Text == "0" && textBox_display.Text != null)
            {
                textBox_display.Text = "5";
            }
            else
            {
                textBox_display.Text = textBox_display.Text + "5";
            }
        }

        private void button6_Click(object sender, EventArgs e)
        {
            if (textBox_display.Text == "0" && textBox_display.Text != null)
            {
                textBox_display.Text = "6";
            }
            else
            {
                textBox_display.Text = textBox_display.Text + "6";
            }
        }

        private void button7_Click(object sender, EventArgs e)
        {
            if (textBox_display.Text == "0" && textBox_display.Text != null)
            {
                textBox_display.Text = "7";
            }
            else
            {
                textBox_display.Text = textBox_display.Text + "7";
            }
        }

        private void button8_Click(object sender, EventArgs e)
        {
            if (textBox_display.Text == "0" && textBox_display.Text != null)
            {
                textBox_display.Text = "8";
            }
            else
            {
                textBox_display.Text = textBox_display.Text + "8";
            }
        }

        private void button9_Click(object sender, EventArgs e)
        {
            if (textBox_display.Text == "" && textBox_display.Text != null)
            {
                textBox_display.Text = "9";
            }
            else
            {
                textBox_display.Text = textBox_display.Text + "9";
            }
        }

        private void button0_Click(object sender, EventArgs e)
        {
            textBox_display.Text = textBox_display.Text + "0";
        }

        private void button_dot_Click(object sender, EventArgs e)
        {
            textBox_display.Text = textBox_display.Text + ".";
        }

        private void button_off_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void button_ce_Click(object sender, EventArgs e)
        {
            textBox_display.Text = "0";
        }

        private void button_add_Click(object sender, EventArgs e)
        {
            FirstNumber = Convert.ToDouble(textBox_display.Text);
            textBox_display.Text = "0";
            Operation = "+";
        }

        private void button_sub_Click(object sender, EventArgs e)
        {
            FirstNumber = Convert.ToDouble(textBox_display.Text);
            textBox_display.Text = "0";
            Operation = "-";
        }

        private void button_mul_Click(object sender, EventArgs e)
        {
            FirstNumber = Convert.ToDouble(textBox_display.Text);
            textBox_display.Text = "0";
            Operation = "*";
        }

        private void button_div_Click(object sender, EventArgs e)
        {
            FirstNumber = Convert.ToDouble(textBox_display.Text);
            textBox_display.Text = "0";
            Operation = "/";
        }

        private void button_equal_Click(object sender, EventArgs e)
        {
            double SecondNumber;
            double Result;

            SecondNumber = Convert.ToDouble(textBox_display.Text);

            if (Operation == "+")
            {
                Result = (FirstNumber + SecondNumber);
                textBox_display.Text = Convert.ToString(Result);
                FirstNumber = Result;
            }
            if (Operation == "-")
            {
                Result = (FirstNumber - SecondNumber);
                textBox_display.Text = Convert.ToString(Result);
                FirstNumber = Result;
            }
            if (Operation == "*")
            {
                Result = (FirstNumber * SecondNumber);
                textBox_display.Text = Convert.ToString(Result);
                FirstNumber = Result;
            }
            if (Operation == "/")
            {
                if (SecondNumber == 0)
                {
                    textBox_display.Text = "Cannot divide by zero";

                }
                else
                {
                    Result = (FirstNumber / SecondNumber);
                    textBox_display.Text = Convert.ToString(Result);
                    FirstNumber = Result;
                }
            }
        }

        private void button_sqrt_Click(object sender, EventArgs e)
        {
            FirstNumber = Convert.ToDouble(textBox_display.Text);
            double Result = 0;
            Result = Math.Sqrt(FirstNumber);
            textBox_display.Text = Result.ToString();
        }

        private void button_log_Click(object sender, EventArgs e)
        {
            FirstNumber = Convert.ToDouble(textBox_display.Text);
            double Result;
            Result = Math.Log(FirstNumber);
            textBox_display.Text = Result.ToString();
        }

        private void button_sin_Click(object sender, EventArgs e)
        {
            FirstNumber = Convert.ToDouble(textBox_display.Text);
            double Result = 0;
            Result = Math.Sin(FirstNumber);
            textBox_display.Text = Result.ToString();
        }

        private void button_cos_Click(object sender, EventArgs e)
        {
            FirstNumber = Convert.ToDouble(textBox_display.Text);
            double Result = 0;
            Result = Math.Cos(FirstNumber);
            textBox_display.Text = Result.ToString();
        }

        private void button_tan_Click(object sender, EventArgs e)
        {
            FirstNumber = Convert.ToDouble(textBox_display.Text);
            double Result = 0;
            Result = Math.Tan(FirstNumber);
            textBox_display.Text = Result.ToString();
        }
    }
}