﻿namespace LV6___Analiza_1
{
    partial class form_kalkulator
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.button1 = new System.Windows.Forms.Button();
            this.button2 = new System.Windows.Forms.Button();
            this.button3 = new System.Windows.Forms.Button();
            this.button4 = new System.Windows.Forms.Button();
            this.button5 = new System.Windows.Forms.Button();
            this.button6 = new System.Windows.Forms.Button();
            this.button7 = new System.Windows.Forms.Button();
            this.button8 = new System.Windows.Forms.Button();
            this.button9 = new System.Windows.Forms.Button();
            this.button0 = new System.Windows.Forms.Button();
            this.textBox_display = new System.Windows.Forms.TextBox();
            this.button_ce = new System.Windows.Forms.Button();
            this.button_dot = new System.Windows.Forms.Button();
            this.button_off = new System.Windows.Forms.Button();
            this.button_add = new System.Windows.Forms.Button();
            this.button_sub = new System.Windows.Forms.Button();
            this.button_mul = new System.Windows.Forms.Button();
            this.button_div = new System.Windows.Forms.Button();
            this.button_sin = new System.Windows.Forms.Button();
            this.button_cos = new System.Windows.Forms.Button();
            this.button_log = new System.Windows.Forms.Button();
            this.button_sqrt = new System.Windows.Forms.Button();
            this.button_tan = new System.Windows.Forms.Button();
            this.button_equal = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(31, 183);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(35, 35);
            this.button1.TabIndex = 0;
            this.button1.Text = "1";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // button2
            // 
            this.button2.Location = new System.Drawing.Point(82, 183);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(35, 35);
            this.button2.TabIndex = 1;
            this.button2.Text = "2";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // button3
            // 
            this.button3.Location = new System.Drawing.Point(134, 183);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(35, 35);
            this.button3.TabIndex = 2;
            this.button3.Text = "3";
            this.button3.UseVisualStyleBackColor = true;
            this.button3.Click += new System.EventHandler(this.button3_Click);
            // 
            // button4
            // 
            this.button4.Location = new System.Drawing.Point(31, 134);
            this.button4.Name = "button4";
            this.button4.Size = new System.Drawing.Size(35, 35);
            this.button4.TabIndex = 3;
            this.button4.Text = "4";
            this.button4.UseVisualStyleBackColor = true;
            this.button4.Click += new System.EventHandler(this.button4_Click);
            // 
            // button5
            // 
            this.button5.Location = new System.Drawing.Point(82, 134);
            this.button5.Name = "button5";
            this.button5.Size = new System.Drawing.Size(35, 35);
            this.button5.TabIndex = 4;
            this.button5.Text = "5";
            this.button5.UseVisualStyleBackColor = true;
            this.button5.Click += new System.EventHandler(this.button5_Click);
            // 
            // button6
            // 
            this.button6.Location = new System.Drawing.Point(134, 134);
            this.button6.Name = "button6";
            this.button6.Size = new System.Drawing.Size(35, 35);
            this.button6.TabIndex = 5;
            this.button6.Text = "6";
            this.button6.UseVisualStyleBackColor = true;
            this.button6.Click += new System.EventHandler(this.button6_Click);
            // 
            // button7
            // 
            this.button7.Location = new System.Drawing.Point(31, 81);
            this.button7.Name = "button7";
            this.button7.Size = new System.Drawing.Size(35, 35);
            this.button7.TabIndex = 6;
            this.button7.Text = "7";
            this.button7.UseVisualStyleBackColor = true;
            this.button7.Click += new System.EventHandler(this.button7_Click);
            // 
            // button8
            // 
            this.button8.Location = new System.Drawing.Point(82, 81);
            this.button8.Name = "button8";
            this.button8.Size = new System.Drawing.Size(35, 35);
            this.button8.TabIndex = 7;
            this.button8.Text = "8";
            this.button8.UseVisualStyleBackColor = true;
            this.button8.Click += new System.EventHandler(this.button8_Click);
            // 
            // button9
            // 
            this.button9.Location = new System.Drawing.Point(134, 81);
            this.button9.Name = "button9";
            this.button9.Size = new System.Drawing.Size(35, 35);
            this.button9.TabIndex = 8;
            this.button9.Text = "9";
            this.button9.UseVisualStyleBackColor = true;
            this.button9.Click += new System.EventHandler(this.button9_Click);
            // 
            // button0
            // 
            this.button0.Location = new System.Drawing.Point(82, 233);
            this.button0.Name = "button0";
            this.button0.Size = new System.Drawing.Size(35, 35);
            this.button0.TabIndex = 9;
            this.button0.Text = "0";
            this.button0.UseVisualStyleBackColor = true;
            this.button0.Click += new System.EventHandler(this.button0_Click);
            // 
            // textBox_display
            // 
            this.textBox_display.Font = new System.Drawing.Font("Microsoft Sans Serif", 18.25F);
            this.textBox_display.Location = new System.Drawing.Point(31, 22);
            this.textBox_display.Multiline = true;
            this.textBox_display.Name = "textBox_display";
            this.textBox_display.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.textBox_display.Size = new System.Drawing.Size(329, 35);
            this.textBox_display.TabIndex = 10;
            this.textBox_display.Text = "0";
            this.textBox_display.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // button_ce
            // 
            this.button_ce.Location = new System.Drawing.Point(382, 22);
            this.button_ce.Name = "button_ce";
            this.button_ce.Size = new System.Drawing.Size(35, 35);
            this.button_ce.TabIndex = 11;
            this.button_ce.Text = "CE";
            this.button_ce.UseVisualStyleBackColor = true;
            this.button_ce.Click += new System.EventHandler(this.button_ce_Click);
            // 
            // button_dot
            // 
            this.button_dot.Location = new System.Drawing.Point(31, 233);
            this.button_dot.Name = "button_dot";
            this.button_dot.Size = new System.Drawing.Size(35, 35);
            this.button_dot.TabIndex = 12;
            this.button_dot.Text = ",";
            this.button_dot.UseVisualStyleBackColor = true;
            this.button_dot.Click += new System.EventHandler(this.button_dot_Click);
            // 
            // button_off
            // 
            this.button_off.Location = new System.Drawing.Point(134, 233);
            this.button_off.Name = "button_off";
            this.button_off.Size = new System.Drawing.Size(35, 35);
            this.button_off.TabIndex = 13;
            this.button_off.Text = "OFF";
            this.button_off.UseVisualStyleBackColor = true;
            this.button_off.Click += new System.EventHandler(this.button_off_Click);
            // 
            // button_add
            // 
            this.button_add.Location = new System.Drawing.Point(215, 81);
            this.button_add.Name = "button_add";
            this.button_add.Size = new System.Drawing.Size(35, 35);
            this.button_add.TabIndex = 14;
            this.button_add.Text = "+";
            this.button_add.UseVisualStyleBackColor = true;
            this.button_add.Click += new System.EventHandler(this.button_add_Click);
            // 
            // button_sub
            // 
            this.button_sub.Location = new System.Drawing.Point(270, 81);
            this.button_sub.Name = "button_sub";
            this.button_sub.Size = new System.Drawing.Size(35, 35);
            this.button_sub.TabIndex = 15;
            this.button_sub.Text = "-";
            this.button_sub.UseVisualStyleBackColor = true;
            this.button_sub.Click += new System.EventHandler(this.button_sub_Click);
            // 
            // button_mul
            // 
            this.button_mul.Location = new System.Drawing.Point(325, 81);
            this.button_mul.Name = "button_mul";
            this.button_mul.Size = new System.Drawing.Size(35, 35);
            this.button_mul.TabIndex = 16;
            this.button_mul.Text = "*";
            this.button_mul.UseVisualStyleBackColor = true;
            this.button_mul.Click += new System.EventHandler(this.button_mul_Click);
            // 
            // button_div
            // 
            this.button_div.Location = new System.Drawing.Point(215, 134);
            this.button_div.Name = "button_div";
            this.button_div.Size = new System.Drawing.Size(35, 35);
            this.button_div.TabIndex = 17;
            this.button_div.Text = "/";
            this.button_div.UseVisualStyleBackColor = true;
            this.button_div.Click += new System.EventHandler(this.button_div_Click);
            // 
            // button_sin
            // 
            this.button_sin.Location = new System.Drawing.Point(215, 183);
            this.button_sin.Name = "button_sin";
            this.button_sin.Size = new System.Drawing.Size(35, 35);
            this.button_sin.TabIndex = 18;
            this.button_sin.Text = "sin";
            this.button_sin.UseVisualStyleBackColor = true;
            this.button_sin.Click += new System.EventHandler(this.button_sin_Click);
            // 
            // button_cos
            // 
            this.button_cos.Location = new System.Drawing.Point(270, 183);
            this.button_cos.Name = "button_cos";
            this.button_cos.Size = new System.Drawing.Size(35, 35);
            this.button_cos.TabIndex = 19;
            this.button_cos.Text = "cos";
            this.button_cos.UseVisualStyleBackColor = true;
            this.button_cos.Click += new System.EventHandler(this.button_cos_Click);
            // 
            // button_log
            // 
            this.button_log.Location = new System.Drawing.Point(325, 134);
            this.button_log.Name = "button_log";
            this.button_log.Size = new System.Drawing.Size(35, 35);
            this.button_log.TabIndex = 20;
            this.button_log.Text = "log";
            this.button_log.UseVisualStyleBackColor = true;
            this.button_log.Click += new System.EventHandler(this.button_log_Click);
            // 
            // button_sqrt
            // 
            this.button_sqrt.Location = new System.Drawing.Point(270, 134);
            this.button_sqrt.Name = "button_sqrt";
            this.button_sqrt.Size = new System.Drawing.Size(35, 35);
            this.button_sqrt.TabIndex = 21;
            this.button_sqrt.Text = "sqrt";
            this.button_sqrt.UseVisualStyleBackColor = true;
            this.button_sqrt.Click += new System.EventHandler(this.button_sqrt_Click);
            // 
            // button_tan
            // 
            this.button_tan.Location = new System.Drawing.Point(325, 183);
            this.button_tan.Name = "button_tan";
            this.button_tan.Size = new System.Drawing.Size(35, 35);
            this.button_tan.TabIndex = 22;
            this.button_tan.Text = "tan";
            this.button_tan.UseVisualStyleBackColor = true;
            this.button_tan.Click += new System.EventHandler(this.button_tan_Click);
            // 
            // button_equal
            // 
            this.button_equal.Location = new System.Drawing.Point(270, 233);
            this.button_equal.Name = "button_equal";
            this.button_equal.Size = new System.Drawing.Size(35, 35);
            this.button_equal.TabIndex = 23;
            this.button_equal.Text = "=";
            this.button_equal.UseVisualStyleBackColor = true;
            this.button_equal.Click += new System.EventHandler(this.button_equal_Click);
            // 
            // form_kalkulator
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(440, 286);
            this.Controls.Add(this.button_equal);
            this.Controls.Add(this.button_tan);
            this.Controls.Add(this.button_sqrt);
            this.Controls.Add(this.button_log);
            this.Controls.Add(this.button_cos);
            this.Controls.Add(this.button_sin);
            this.Controls.Add(this.button_div);
            this.Controls.Add(this.button_mul);
            this.Controls.Add(this.button_sub);
            this.Controls.Add(this.button_add);
            this.Controls.Add(this.button_off);
            this.Controls.Add(this.button_dot);
            this.Controls.Add(this.button_ce);
            this.Controls.Add(this.textBox_display);
            this.Controls.Add(this.button0);
            this.Controls.Add(this.button9);
            this.Controls.Add(this.button8);
            this.Controls.Add(this.button7);
            this.Controls.Add(this.button6);
            this.Controls.Add(this.button5);
            this.Controls.Add(this.button4);
            this.Controls.Add(this.button3);
            this.Controls.Add(this.button2);
            this.Controls.Add(this.button1);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "form_kalkulator";
            this.ShowIcon = false;
            this.Text = "Znanstveni kalkulator";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.Button button3;
        private System.Windows.Forms.Button button4;
        private System.Windows.Forms.Button button5;
        private System.Windows.Forms.Button button6;
        private System.Windows.Forms.Button button7;
        private System.Windows.Forms.Button button8;
        private System.Windows.Forms.Button button9;
        private System.Windows.Forms.Button button0;
        private System.Windows.Forms.TextBox textBox_display;
        private System.Windows.Forms.Button button_ce;
        private System.Windows.Forms.Button button_dot;
        private System.Windows.Forms.Button button_off;
        private System.Windows.Forms.Button button_add;
        private System.Windows.Forms.Button button_sub;
        private System.Windows.Forms.Button button_mul;
        private System.Windows.Forms.Button button_div;
        private System.Windows.Forms.Button button_sin;
        private System.Windows.Forms.Button button_cos;
        private System.Windows.Forms.Button button_log;
        private System.Windows.Forms.Button button_sqrt;
        private System.Windows.Forms.Button button_tan;
        private System.Windows.Forms.Button button_equal;
    }
}

