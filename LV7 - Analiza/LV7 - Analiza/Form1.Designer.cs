﻿namespace LV7___Analiza
{
    partial class TicTacToe
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label_player1 = new System.Windows.Forms.Label();
            this.textBox_player1 = new System.Windows.Forms.TextBox();
            this.label_player2 = new System.Windows.Forms.Label();
            this.textBox_player2 = new System.Windows.Forms.TextBox();
            this.button1 = new System.Windows.Forms.Button();
            this.button2 = new System.Windows.Forms.Button();
            this.button3 = new System.Windows.Forms.Button();
            this.button4 = new System.Windows.Forms.Button();
            this.button5 = new System.Windows.Forms.Button();
            this.button6 = new System.Windows.Forms.Button();
            this.button7 = new System.Windows.Forms.Button();
            this.button8 = new System.Windows.Forms.Button();
            this.button9 = new System.Windows.Forms.Button();
            this.button_start = new System.Windows.Forms.Button();
            this.button10 = new System.Windows.Forms.Button();
            this.label_turn = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // label_player1
            // 
            this.label_player1.Location = new System.Drawing.Point(13, 13);
            this.label_player1.Name = "label_player1";
            this.label_player1.Size = new System.Drawing.Size(49, 13);
            this.label_player1.TabIndex = 0;
            this.label_player1.Text = "Player 1:";
            // 
            // textBox_player1
            // 
            this.textBox_player1.Location = new System.Drawing.Point(68, 10);
            this.textBox_player1.Name = "textBox_player1";
            this.textBox_player1.Size = new System.Drawing.Size(100, 20);
            this.textBox_player1.TabIndex = 1;
            // 
            // label_player2
            // 
            this.label_player2.AutoSize = true;
            this.label_player2.Location = new System.Drawing.Point(203, 13);
            this.label_player2.Name = "label_player2";
            this.label_player2.Size = new System.Drawing.Size(48, 13);
            this.label_player2.TabIndex = 2;
            this.label_player2.Text = "Player 2:";
            // 
            // textBox_player2
            // 
            this.textBox_player2.Location = new System.Drawing.Point(257, 10);
            this.textBox_player2.Name = "textBox_player2";
            this.textBox_player2.Size = new System.Drawing.Size(100, 20);
            this.textBox_player2.TabIndex = 3;
            // 
            // button1
            // 
            this.button1.Font = new System.Drawing.Font("Microsoft Sans Serif", 40.25F);
            this.button1.Location = new System.Drawing.Point(50, 87);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(75, 75);
            this.button1.TabIndex = 4;
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button_click);
            // 
            // button2
            // 
            this.button2.Font = new System.Drawing.Font("Microsoft Sans Serif", 40.25F);
            this.button2.Location = new System.Drawing.Point(150, 87);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(75, 75);
            this.button2.TabIndex = 5;
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.button_click);
            // 
            // button3
            // 
            this.button3.Font = new System.Drawing.Font("Microsoft Sans Serif", 40.25F);
            this.button3.Location = new System.Drawing.Point(250, 87);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(75, 75);
            this.button3.TabIndex = 6;
            this.button3.UseVisualStyleBackColor = true;
            this.button3.Click += new System.EventHandler(this.button_click);
            // 
            // button4
            // 
            this.button4.Font = new System.Drawing.Font("Microsoft Sans Serif", 40.25F);
            this.button4.Location = new System.Drawing.Point(50, 187);
            this.button4.Name = "button4";
            this.button4.Size = new System.Drawing.Size(75, 75);
            this.button4.TabIndex = 7;
            this.button4.UseVisualStyleBackColor = true;
            this.button4.Click += new System.EventHandler(this.button_click);
            // 
            // button5
            // 
            this.button5.Font = new System.Drawing.Font("Microsoft Sans Serif", 40.25F);
            this.button5.Location = new System.Drawing.Point(150, 187);
            this.button5.Name = "button5";
            this.button5.Size = new System.Drawing.Size(75, 75);
            this.button5.TabIndex = 8;
            this.button5.UseVisualStyleBackColor = true;
            this.button5.Click += new System.EventHandler(this.button_click);
            // 
            // button6
            // 
            this.button6.Font = new System.Drawing.Font("Microsoft Sans Serif", 40.25F);
            this.button6.Location = new System.Drawing.Point(250, 187);
            this.button6.Name = "button6";
            this.button6.Size = new System.Drawing.Size(75, 75);
            this.button6.TabIndex = 9;
            this.button6.UseVisualStyleBackColor = true;
            this.button6.Click += new System.EventHandler(this.button_click);
            // 
            // button7
            // 
            this.button7.Font = new System.Drawing.Font("Microsoft Sans Serif", 40.25F);
            this.button7.Location = new System.Drawing.Point(50, 287);
            this.button7.Name = "button7";
            this.button7.Size = new System.Drawing.Size(75, 75);
            this.button7.TabIndex = 10;
            this.button7.UseVisualStyleBackColor = true;
            this.button7.Click += new System.EventHandler(this.button_click);
            // 
            // button8
            // 
            this.button8.Font = new System.Drawing.Font("Microsoft Sans Serif", 40.25F);
            this.button8.Location = new System.Drawing.Point(150, 287);
            this.button8.Name = "button8";
            this.button8.Size = new System.Drawing.Size(75, 75);
            this.button8.TabIndex = 11;
            this.button8.UseVisualStyleBackColor = true;
            this.button8.Click += new System.EventHandler(this.button_click);
            // 
            // button9
            // 
            this.button9.Font = new System.Drawing.Font("Microsoft Sans Serif", 40.25F);
            this.button9.Location = new System.Drawing.Point(250, 287);
            this.button9.Name = "button9";
            this.button9.Size = new System.Drawing.Size(75, 75);
            this.button9.TabIndex = 12;
            this.button9.UseVisualStyleBackColor = true;
            this.button9.Click += new System.EventHandler(this.button_click);
            // 
            // button_start
            // 
            this.button_start.Location = new System.Drawing.Point(93, 45);
            this.button_start.Name = "button_start";
            this.button_start.Size = new System.Drawing.Size(75, 23);
            this.button_start.TabIndex = 13;
            this.button_start.Text = "New Game";
            this.button_start.UseVisualStyleBackColor = true;
            this.button_start.Click += new System.EventHandler(this.button_start_Click);
            // 
            // button10
            // 
            this.button10.Location = new System.Drawing.Point(206, 45);
            this.button10.Name = "button10";
            this.button10.Size = new System.Drawing.Size(75, 23);
            this.button10.TabIndex = 14;
            this.button10.Text = "Exit";
            this.button10.UseVisualStyleBackColor = true;
            this.button10.Click += new System.EventHandler(this.button10_Click);
            // 
            // label_turn
            // 
            this.label_turn.AutoSize = true;
            this.label_turn.Location = new System.Drawing.Point(161, 376);
            this.label_turn.Name = "label_turn";
            this.label_turn.Size = new System.Drawing.Size(32, 13);
            this.label_turn.TabIndex = 15;
            this.label_turn.Text = "Turn:";
            // 
            // TicTacToe
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(378, 407);
            this.ControlBox = false;
            this.Controls.Add(this.label_turn);
            this.Controls.Add(this.button10);
            this.Controls.Add(this.button_start);
            this.Controls.Add(this.button9);
            this.Controls.Add(this.button8);
            this.Controls.Add(this.button7);
            this.Controls.Add(this.button6);
            this.Controls.Add(this.button5);
            this.Controls.Add(this.button4);
            this.Controls.Add(this.button3);
            this.Controls.Add(this.button2);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.textBox_player2);
            this.Controls.Add(this.label_player2);
            this.Controls.Add(this.textBox_player1);
            this.Controls.Add(this.label_player1);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "TicTacToe";
            this.ShowIcon = false;
            this.Text = "Tic Tac Toe";
            this.Load += new System.EventHandler(this.TicTacToe_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label_player1;
        private System.Windows.Forms.TextBox textBox_player1;
        private System.Windows.Forms.Label label_player2;
        private System.Windows.Forms.TextBox textBox_player2;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.Button button3;
        private System.Windows.Forms.Button button4;
        private System.Windows.Forms.Button button5;
        private System.Windows.Forms.Button button6;
        private System.Windows.Forms.Button button7;
        private System.Windows.Forms.Button button8;
        private System.Windows.Forms.Button button9;
        private System.Windows.Forms.Button button_start;
        private System.Windows.Forms.Button button10;
        private System.Windows.Forms.Label label_turn;
    }
}

