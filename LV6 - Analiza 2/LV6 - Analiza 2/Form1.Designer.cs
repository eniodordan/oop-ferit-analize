﻿namespace LV6___Analiza_2
{
    partial class form_vjesalo
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label_word = new System.Windows.Forms.Label();
            this.label_pokusaji = new System.Windows.Forms.Label();
            this.textBox_pokusaj = new System.Windows.Forms.TextBox();
            this.button_pokusaj = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // label_word
            // 
            this.label_word.AutoSize = true;
            this.label_word.Font = new System.Drawing.Font("Microsoft Sans Serif", 20.25F);
            this.label_word.Location = new System.Drawing.Point(143, 93);
            this.label_word.Name = "label_word";
            this.label_word.Size = new System.Drawing.Size(0, 31);
            this.label_word.TabIndex = 0;
            this.label_word.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label_pokusaji
            // 
            this.label_pokusaji.AutoSize = true;
            this.label_pokusaji.Location = new System.Drawing.Point(12, 187);
            this.label_pokusaji.Name = "label_pokusaji";
            this.label_pokusaji.Size = new System.Drawing.Size(131, 13);
            this.label_pokusaji.TabIndex = 1;
            this.label_pokusaji.Text = "Broj preostalih pokušaja: 8";
            // 
            // textBox_pokusaj
            // 
            this.textBox_pokusaj.Location = new System.Drawing.Point(129, 30);
            this.textBox_pokusaj.Name = "textBox_pokusaj";
            this.textBox_pokusaj.Size = new System.Drawing.Size(58, 20);
            this.textBox_pokusaj.TabIndex = 2;
            // 
            // button_pokusaj
            // 
            this.button_pokusaj.Location = new System.Drawing.Point(232, 28);
            this.button_pokusaj.Name = "button_pokusaj";
            this.button_pokusaj.Size = new System.Drawing.Size(75, 23);
            this.button_pokusaj.TabIndex = 3;
            this.button_pokusaj.Text = "Pokušaj";
            this.button_pokusaj.UseVisualStyleBackColor = true;
            this.button_pokusaj.Click += new System.EventHandler(this.button_pokusaj_Click);
            // 
            // form_vjesalo
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(434, 209);
            this.Controls.Add(this.button_pokusaj);
            this.Controls.Add(this.textBox_pokusaj);
            this.Controls.Add(this.label_pokusaji);
            this.Controls.Add(this.label_word);
            this.MaximizeBox = false;
            this.MaximumSize = new System.Drawing.Size(450, 248);
            this.MinimizeBox = false;
            this.MinimumSize = new System.Drawing.Size(450, 248);
            this.Name = "form_vjesalo";
            this.ShowIcon = false;
            this.Text = "Vješalo";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label_word;
        private System.Windows.Forms.Label label_pokusaji;
        private System.Windows.Forms.TextBox textBox_pokusaj;
        private System.Windows.Forms.Button button_pokusaj;
    }
}

